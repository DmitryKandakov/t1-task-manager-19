package ru.t1.dkandakov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.19.1");
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show version.";
    }

}