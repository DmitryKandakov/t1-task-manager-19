package ru.t1.dkandakov.tm.command.user;

import ru.t1.dkandakov.tm.api.service.IAuthService;
import ru.t1.dkandakov.tm.api.service.IUserService;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(User user) {
        System.out.println(user.getLogin() + " " + user.getEmail());
    }

}
