package ru.t1.dkandakov.tm.command.task;

import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Start task by id.";
    }

}
