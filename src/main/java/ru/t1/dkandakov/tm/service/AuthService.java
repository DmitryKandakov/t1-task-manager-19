package ru.t1.dkandakov.tm.service;

import ru.t1.dkandakov.tm.api.service.IAuthService;
import ru.t1.dkandakov.tm.api.service.IUserService;
import ru.t1.dkandakov.tm.exception.user.AccessDeniedException;
import ru.t1.dkandakov.tm.exception.user.LoginEmptyException;
import ru.t1.dkandakov.tm.exception.user.PasswordEmptyException;
import ru.t1.dkandakov.tm.model.User;
import ru.t1.dkandakov.tm.util.HashUtil;

public final class AuthService extends AbstractService<User, IUserService> implements IAuthService {

    private String userId;

    public AuthService(final IUserService userService) {
        super(userService);
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return repository.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = repository.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = repository.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}