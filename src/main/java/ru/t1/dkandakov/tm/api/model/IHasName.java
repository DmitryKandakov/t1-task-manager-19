package ru.t1.dkandakov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}