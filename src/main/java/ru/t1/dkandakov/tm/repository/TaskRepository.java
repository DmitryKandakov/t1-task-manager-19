package ru.t1.dkandakov.tm.repository;

import ru.t1.dkandakov.tm.api.repository.ITaskRepository;
import ru.t1.dkandakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}